# EBook-Utils

## Overview
Image used to convert EPUB files to MOBI files (to be read on a Kindle).

## Image Content
The image is based on Ubuntu and uses [calibre](https://github.com/kovidgoyal/calibre) to convert the files.

For east use, a few scripts are built in the image:
 - convert_all.sh <directory_path>

   Will convert all files with the EPUB extension from the provided directory.

 - convert_one.sh <file_path>

   Will convert the provided EPUB file.

## Directory Content
In this directory are a few more scripts:
 - build.sh

   Script used to build the image; it is not needed if the image was pulled from [Docker Hub](https://hub.docker.com/r/calestar/ebook-utils/).

 - run.sh <file_path/directory_path>

   Helper to use the image, will call the right image script based on the argument passed to it.