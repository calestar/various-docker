#!/bin/bash

if [ ! $# = 1 ]; then
  echo "Expecting exactly one argument: the path (or file) to serve"
  exit
fi

if [ -d $1 ]; then
  command="convert_all.sh /data/"
  directory=$1
elif [ -f $1 ]; then
  filename=$(basename $1)
  extension=${filename##*.}
  if [ ! $extension = "epub" ]; then
    echo "Error: Only .epub files are supported."
    exit
  fi

  command="convert_one.sh /data/$filename"
  directory=$(dirname $1)
else
  echo "Error: $1 is neither a file nor a directory."
  exit
fi

directory=$(realpath $directory)

docker run \
  --rm \
  -v $directory:/data \
  -i \
  -t calestar/ebook-utils \
  $command
