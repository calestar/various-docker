#!/bin/bash

function usage
{
  echo "Error: $1"
  echo "Usage: convert_all.sh /some/directory "
}

shopt -s nullglob

if [ $# -lt 1 ]; then
  usage "Missing argument"
elif [ $# -gt 1 ]; then
  usage "Too many arguments"
elif [ ! -d "$1" ]; then
  usage "Directory '$1' does not exist"
else
  echo "Processing epub files in '$1'"
  for file in $1/*.epub
  do
    ebook-convert "$file" "${file%.epub}.mobi"
  done
fi

shopt -u nullglob

