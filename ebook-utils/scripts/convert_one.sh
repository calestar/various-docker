#!/bin/bash

function usage
{
  echo "Error: $1"
  echo "Usage: convert_one.sh /some/directory/file.epub "
}

shopt -s nullglob

if [ $# -lt 1 ]; then
  usage "Missing argument"
elif [ $# -gt 1 ]; then
  usage "Too many arguments"
elif [ ! -f "$1" ]; then
  usage "File '$1' does not exist"
else
  filename=$(basename $1)
  directory=$(dirname $1)
  extension=${filename##*.}
  if [ ! $extension = "epub" ]; then
    usage "Only .epub files are supported"
  else
    echo "Processing file '$1'"
    ebook-convert "$1" "${1%.epub}.mobi"
  fi
fi

shopt -u nullglob

